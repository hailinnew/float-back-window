package com.js.myapplication;

import static com.col.flutterplugin.float_button_overlay.FloatButtonOverlayPlugin.ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE;
import static com.col.flutterplugin.float_button_overlay.utils.Constants.TAG;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.col.flutterplugin.float_button_overlay.FloatButtonOverlayPlugin;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((TextView) (findViewById(R.id.helloworld))).setText(System.currentTimeMillis() + "");
        boolean permission = FloatButtonOverlayPlugin.checkPermission(this);
        if (permission) {
            FloatButtonOverlayPlugin.openOverlay(this, this.getPackageName(), this.getClass().getName());
        }

//        boolean isAvailable = isAvilible(this,"com.signway.assist");
//        if (isAvailable) {
//            FloatButtonOverlayPlugin.startAppActivity(this,"com.signway.assist","com.signway.assist.MainActivity");
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                Log.e(TAG, "Float Button Overlay will not work without 'Can Draw Over Other Apps' permission");
                Toast.makeText(this, "Float Button Overlay will not work without 'Can Draw Over Other Apps' permission", Toast.LENGTH_LONG).show();
            } else {
                FloatButtonOverlayPlugin.openOverlay(this, this.getPackageName(), this.getClass().getName());
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FloatButtonOverlayPlugin.closeOverlay(this, this.getPackageName(), this.getClass().getName());
    }

    public void startApp(View view) {
        boolean isAvailable = FloatButtonOverlayPlugin.isPackageAvailable(this, "com.signway.assist");
        if (isAvailable) {
            FloatButtonOverlayPlugin.startAppActivity(this, "com.signway.assist", "com.signway.assist.MainActivity");
        } else if (FloatButtonOverlayPlugin.isPackageAvailable(this, "com.rkpower")) {
            FloatButtonOverlayPlugin.startAppActivity(this, "com.rkpower", "com.rkpower.MainActivity");
        }
    }
}